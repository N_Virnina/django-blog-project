from django.urls import path
from . import views

app_name = 'blog'  # prefix for url

urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('fake_data', views.mock_data, name='mock_data'),
    path('<int:id>/', views.one_post, name='one_post'),
    path('<str:category_country>/', views.get_category, name='get_category')
]
