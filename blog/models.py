from django.db import models

# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=200)
    category_country = models.CharField(max_length=200)  # country
    city = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    published_date = models.DateTimeField()
    text = models.TextField()

    def publish(self):
        self.save()

    def __str__(self):
        return f'{self.title} (posted by {self.author})'
