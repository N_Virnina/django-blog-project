from django.shortcuts import render
from django.http import HttpResponse
from .models import Post


# Create your views here.


def mock_data(request):
    Post.objects.all().delete()

    Post.objects.create(title='24 hours in Frankfurt',
                        category_country='Germany',
                        city='Frankfurt',
                        author='Greek Gal',
                        published_date='2011-04-28 18:23',
                        text='Walk to the other side of the river (from the railway station) and stroll along the '
                             'river. There are also some good museums in that area.')

    Post.objects.create(title='Above the gorge of the Ardeche',
                        category_country='France',
                        city='Languedoc-Roussillon',
                        author='rbcameron',
                        published_date='2020-09-26 10:18',
                        text='After Mende we spent two more days wending our way through the quiet, green hills of '
                             'the Cevennes. Then yesterday we stumbled into Tourist France. The first hint was the '
                             'flotilla of multi-coloured kayaks floating down the Ardèche river.')

    Post.objects.create(title='2012 Marathon in Italy',
                        category_country='Italy',
                        city='Rome',
                        author='Janeen Jennings',
                        published_date='2011-10-11 11:21',
                        text='Traveling from U.S. for Marathon in Rome, March 2012 - Any other marathoners out there?')

    Post.objects.create(title='München - Südkoreanisches BBQ',
                        category_country='Germany',
                        city='Munich',
                        author='Twag',
                        published_date='2019-09-25 20:59',
                        text='Nachdem ich 2002 rund 6 Wochen ein Projekt in Südkorea gemacht habe und dort fast jeden '
                             'Tag zum südkoreanischen BBQ eingeladen war, habe ich immer wieder bei uns nach so einem '
                             'Restaurant gesucht und bin nun in München fündig geworden.')

    Post.objects.create(title='The Journey begins........ Segovia',
                        category_country='Spain',
                        city='Segovia',
                        author='D MJ Binkley',
                        published_date='2018-08-24 10:10',
                        text='Not all those who wander are lost” ~ J.R.R. Tolkien. It was simply bugging the hell out '
                             'of us....we would talk about it, imagine it, even attempt to make plans in the hopes '
                             'that we could travel....somewhere.')

    Post.objects.create(title='Cost of Beer Around the World',
                        category_country='Germany',
                        city='Munich',
                        author='Gerry Kataoka',
                        published_date='2021-03-02 23:23',
                        text='Before becoming a more serious sparkling wine and red wine lover, I drank my fair share '
                             'of beer.')

    Post.objects.create(title='Chateauneuf du Pape',
                        category_country='France',
                        city='Chateauneuf-du-Pape',
                        author='Mon en Fran',
                        published_date='2016-08-25 17:40',
                        text='Woensdag 25/8/2021 Chateauneuf du Pape Bomma is gevallen met de fiets. Maar leeft nog. '
                             'Gisteren, dinsdag rond de middag op een afgepijlde 30 km lange rondrit door de '
                             'wijngaarden rond Chateauneuf du Pape kreeg ze in een afdaling een klapband op het '
                             'achterwiel van haar fiets.')

    Post.objects.create(title='Krakow illuminated',
                        category_country='Poland',
                        city='Krakow',
                        author='BevsBlogg',
                        published_date='2011-04-22 21:29',
                        text='The sky was full of lightning, the thunder was rumbling and the rain was pouring but '
                             'that did not stop Tom from continuing his tour showing the beautiful arts & crafts '
                             'architecture in Krakow.')

    return HttpResponse('Fake data have been created successfully.')


def post_list(request):
    posts = Post.objects.filter().order_by('-published_date')
    return render(request, 'blog/post_list.html', {'posts': posts})


def one_post(request, id):
    post = Post.objects.get(pk=id)
    return render(request, 'blog/post.html', context={'post': post})


def get_category(request, category_country):
    category_posts = Post.objects.filter(category_country=category_country)
    return render(request, 'blog/category.html', context={'category_posts': category_posts})
